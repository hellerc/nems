#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  3 17:02:00 2017

@author: shofer
"""

#Init file for nems/fitters

import nems.fitters.fitters

__all__=['fitters']