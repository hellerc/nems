""" Store any sensitive settings files in this directory, and keep
    the directory listed in .gitignore.
    
"""

# TODO: Keep config files separate, or put in one file but
#       define as separate classes?

STORAGE_DEFAULTS = {
        'DIRECTORY_ROOT' : '/auto/data/code/'
        }
