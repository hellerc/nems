#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 12 17:13:31 2017

@author: hellerc
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.cluster.vq import whiten

def whiten(X):
    U, S, V = np.linalg.svd(X, full_matrices=False)
    U *= np.sqrt(X.shape[0])
    return U

from itertools import product

monomials = lambda n, d: [t for t in product(range(d+1), repeat=n) if sum(t)<=d and sum(t) > 0]
expand = lambda X, d: np.apply_along_axis(lambda x, mn: np.prod(np.power(x, mn), axis=1), 1, X, monomials(X.shape[1], d))



t = np.linspace(0, 2*np.pi, 5000)
x1 = np.sin(t)+2*np.power(np.cos(11.*t), 2)
x2 = np.cos(11.*t)
X = np.column_stack((x1, x2))    


derivative = lambda X: X[1:, :]-X[:-1, :]


plt.subplot(1, 2, 1)
plt.plot(X[:,1], X[:,0], '.') 

Y = whiten(expand(X, 2))
U, S, V = np.linalg.svd(derivative(Y), full_matrices=False)

plt.subplot(1, 2, 2)
Z = Y[:].dot(V[-1,:])
plt.plot(Z)